/**
 * Module for controlling a KYX-2481AS
 * Quad-7-Segment Field with doublepoint and point on each digit
 * @param[in| clk The input clock
 * @param[in] value The full value for the whole bitfield 16(bit) [15:12, 11:8, 7:4, 3:0]
 * @param[in] points control for enabling the points [DP2,COLON,DP3,DP4]
 * @param[out] pins_select select pins for the digit [DP1, DP2, DP3, DP4]
 * @param[out] pins_leds segment leds: \n
 *    -  0 \n
 * 5 | | 1 \n
 *  6 -    \n
 * 4 | | 2 \n
 *    - 3  \n
 * @param[out] pin_dot pin for the dot leds
 *
 * @note
 * The digits on the display are [DP1(X---), DP2(-X--), DP3(--X-), DP4(---X)]
 */
module rl_kyx2481as (
                     input        clk,
                     input [15:0] value,
                     input [3:0]  points,
                     output [3:0] pins_select,
                     output [6:0] pins_leds,
                     output pin_points
                     );
   wire [27:0]                    w_led_out;
   reg [1:0]                      digit_num = 0;
   reg [3:0]                      digit_select_prep = 0;
   reg [27:0]                     digit_current_state = 0;
   reg                            points_prep = 0;

   rl_vis_7seg_enc digit0_ctrl(
                               .value(value[3:0]),
                               .led_out(w_led_out[6:0])
                               );

   rl_vis_7seg_enc digit1_ctrl(
                               .value(value[7:4]),
                               .led_out(w_led_out[13:7])
                               );

   rl_vis_7seg_enc digit2_ctrl(
                               .value(value[11:8]),
                               .led_out(w_led_out[20:14])
                               );

   rl_vis_7seg_enc digit3_ctrl(
                               .value(value[15:12]),
                               .led_out(w_led_out[27:21])
                               );

   always @(posedge clk)
     begin
        digit_select_prep <= ~(1 << digit_num);
        points_prep <= points >> digit_num;
        digit_current_state <= w_led_out >> (digit_num * 7);
        digit_num <= digit_num + 1;
     end

   assign pins_select = digit_select_prep;
   assign pins_leds = digit_current_state[6:0];
   assign pin_points = points_prep;
endmodule
