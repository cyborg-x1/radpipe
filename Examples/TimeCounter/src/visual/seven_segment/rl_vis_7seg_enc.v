/**
 * Seven Segment Encoder
 * @param value 4Bit value for 0-9A-F
 * @param led_out LED output variable
 *
 *    -  0
 * 5 | | 1
 *  6 -
 * 4 | | 2
 *    - 3
 */
module rl_vis_7seg_enc (
                        input [3:0]      value,
                        output reg [6:0] led_out
                        );

   always @(*)
     begin
        case ({value[3], value[2], value[1],value[0]})
          4'b0000  : led_out = 7'b0111111;
          4'b0001  : led_out = 7'b0000110;
          4'b0010  : led_out = 7'b1011011;
          4'b0011  : led_out = 7'b1001111;
          4'b0100  : led_out = 7'b1100110;
          4'b0101  : led_out = 7'b1101101;
          4'b0110  : led_out = 7'b1111101;
          4'b0111  : led_out = 7'b0000111;
          4'b1000  : led_out = 7'b1111111;
          4'b1001  : led_out = 7'b1101111;
          4'b1010  : led_out = 7'b1110111;
          4'b1011  : led_out = 7'b1111100;
          4'b1100  : led_out = 7'b0111001;
          4'b1101  : led_out = 7'b1011110;
          4'b1110  : led_out = 7'b1111001;
          4'b1111  : led_out = 7'b1110001;

          default : led_out = 7'b1001001;
        endcase
     end
endmodule
