module rl_vis_7seg_enc_fixture;
   reg [3:0] value;
   wire [6:0] led_out;
   rl_vis_7seg_enc u0(value, led_out);

   initial begin
      value = 15;
      repeat(16)
        begin
           #10 value = value + 1;
           $display("%d:%d", value,   led_out);
        end
      #100 $finish;
   end
endmodule
