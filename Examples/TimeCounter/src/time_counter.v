module time_counter (
                     input        clk,
                     output [6:0] PINS_SEGMENTS,
                     output [3:0] PINS_SEGMENT_SELECT,
                     output       PIN_POINTS,
                     input [3:0]  KEY
                     );


   wire                           clock_2Hz;
   rl_clk_div #(.div(250)) m_clock_2Hz
     (
      .clk_in(clk),
      .clk_out(clock_2Hz)
      );

   reg [3:0]                      count_sec = 0;
   reg [2:0]                      count_dsec = 0;
   reg [3:0]                      count_min = 0;
   reg [2:0]                      count_dmin = 0;
   reg [3:0]                      points_out = 0;
   reg [2:0]                      points = 0;

   /* TIME COUNTER */
   reg [15:0]                     time_value = 2;

   always @(posedge clock_2Hz) // Execute this at positive clock edge
     begin
        count_sec = count_sec + 1;
        if (count_sec >= 10)
          begin
             count_sec = 0;
             count_dsec = count_dsec + 1;
          end

        if (count_dsec >= 6)
          begin
             count_dsec = 0;
             count_min = count_min + 1;
          end

        if (count_min >= 10)
          begin
             count_min = 0;
             count_dmin = count_dmin + 1;
          end

        if (count_dmin >= 6)
          begin
             count_dmin = 0;
             points = points + 1;
          end

        time_value <= count_sec + (count_dsec << 4) + (count_min << 8) + (count_dmin << 12);

        points_out <= (points[2] << 3) + (count_sec[0] << 2) +(points[1] << 1) + points[0];
     end

   /* 7 Segment display */
   rl_kyx2481as m_seven_segments(
                                 .clk(clk),
                                 .value(time_value),
                                 .points(points_out),
                                 .pins_select(PINS_SEGMENT_SELECT),
                                 .pins_leds(PINS_SEGMENTS),
                                 .pin_points(PIN_POINTS)
                                 );


endmodule // time_counter
