module rl_clk_div (
                   input      clk_in,
                   output reg clk_out
                   );
   parameter div = 2;
   reg [31:0]                 div_counter = 0;
   reg                        prep_clk_out = 0;

   always @(posedge clk_in)
     begin
        if(div_counter <= div)
          begin
             div_counter <= div_counter + 1;
          end
        else
          begin
             div_counter <= 0;
             prep_clk_out = ~prep_clk_out;
          end
        clk_out <= prep_clk_out;
     end
endmodule
