# language: en
Feature: Time Counter with Seven Segment Displays
  @vcd
  Scenario: TimeCounter counting
    Given is the verilog top module "time_counter" in the file "src/time_counter.v" and the paths
      | path                     |
      | src/clock                |
      | src/visual               |
      | src/visual/parts         |
      | src/visual/seven_segment |
    When "clk" is ticked "100" times
