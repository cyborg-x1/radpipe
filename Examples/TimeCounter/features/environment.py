from behave import *
from os.path import basename, splitext
from os import scandir
from allure_commons.types import AttachmentType
import allure
from urllib.parse import quote

def before_all(_context):
    pass

def before_feature(_context, feature):
    feature.scenario_number = 0


def before_scenario(context, _scenario):
    base, _ext = splitext(basename(context.feature.filename))
    context.vcd_file = "{}{}.vcd".format(
        base,
        context.feature.scenario_number
    )
    context.feature.scenario_number += 1

def after_scenario(context, _scenario):
    if 'vcd' in context.tags:
        context.sim.stop_vcd_trace
        vcd_data = open(context.vcd_file, "r").read()


        vcd_viewer = """
<html>
    <head>
        <meta charset="utf-8"/>
        <title>My D3js Playground</title>
    </head>
    <style>

     html, body {
         height: 100%;
         margin: 0;
     }

     .svg_canvas {
         display: block;
         margin: 0 auto;
         background-color: gray;

     }

     .population_line {
         fill: none;
         stroke: white;
         stroke-width: 2;
     }

     #diagrams {
         width: 100%;
         height: 100%;
         margin: 0;
         background-color: red;
     }

    </style>
    <body>
        <a href="javascript:void(0)" id="dlbtn">
        <button>Save Value Change Dump File (vcd)</button></a>
        <div id="diagrams" data-vcd="VCDDATA" ></div>
    </body>
    <script src="/d3.min.js"></script>
    <script src="/logic.js"></script>
    <script>
     setTimeout("createVCDDownload()");
     function createVCDDownload() {
         var dlbtn = document.getElementById("dlbtn");
         var vcdData = document.getElementById("diagrams").getAttribute("data-vcd");
         var file = new Blob([unescape(vcdData)], {type: 'text/plain'});
         dlbtn.href = URL.createObjectURL(file);
         dlbtn.download = "VCDFILENAME";
     }
    </script>
</html>
"""
        vcd_viewer = vcd_viewer.replace('VCDDATA', quote(vcd_data))
        vcd_viewer = vcd_viewer.replace('VCDFILENAME', context.vcd_file)

        allure.attach(vcd_viewer,
                      name='Value Change Dump File Viewer',
                      extension='html')

def after_feature(context, feature):
    for filename in scandir(context.config.junit_directory):
        print(filename)

def after_all(context):
    pass
