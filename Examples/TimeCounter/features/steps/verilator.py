from behave import *
import pyverilator
import os


use_step_matcher("re")

sims = {}

@given(u'is the verilog top module "(?P<top_module>.*)" in the file "(?P<top_verilog_file>.*)"(?P<paths> and the paths)?')
def step_impl(context, top_module, top_verilog_file, paths):
    verilog_path = []
    if paths:
        verilog_path = [line['path'] for line in context.table]

    if top_module not in sims:
        sims[top_module] = pyverilator.PyVerilator.build(top_verilog_file,
                                                         verilog_path)
    context.sim = sims[top_module]

    if context.config.junit:
        if 'vcd' in context.tags:
            context.sim.start_vcd_trace(context.vcd_file)



@then(u'bit "(?P<bit>[0-9]+)" of "(?P<io>.+)" shall be "(?P<state>[0-1])"')
def step_impl(context, bit, io, state):
    out = context.sim.io[io]
    bit_mask = 1 << int(bit)
    masked = out & bit_mask
    bit_set = masked > 0
    bit_state_required = bool(int(state))

    assert bit_state_required == bit_set

@when(u'"(?P<io>.+)" is ticked(?: "(?P<times>[0-9]+)" times)?')
def step_impl(context, io, times):
    if not times:
        times = 1

    for _i in range(int(times)):
        context.sim.io[io] = 1
        context.sim.io[io] = 0

@when(u'"(?P<io>.*)" is set to "(?P<state>[0-9]+)"')
def step_impl(context, io, state):
    context.sim.io[io] = int(state)
