# language: en
Feature: Seven Segment Encoder Verilog Module
  The goal is to be able to convert a 4 bit register
  to a seven segment display output.

  The bit numbers for the leds are as following:
      -  0
   5 | | 1
    6 -
   4 | | 2
      - 3
  @blocker
  Scenario Outline: Seven Segment Module Encoder
    Given is the verilog top module "rl_vis_7seg_enc" in the file "src/visual/seven_segment/rl_vis_7seg_enc.v"
    When "value" is set to "<input>"
    Then bit "0" of "led_out" shall be "<led_0>"
    And  bit "1" of "led_out" shall be "<led_1>"
    And  bit "2" of "led_out" shall be "<led_2>"
    And  bit "3" of "led_out" shall be "<led_3>"
    And  bit "4" of "led_out" shall be "<led_4>"
    And  bit "5" of "led_out" shall be "<led_5>"
    And  bit "6" of "led_out" shall be "<led_6>"

  Examples:
    | input | led_6 | led_5 | led_4 | led_3 | led_2 | led_1 | led_0 |
    |     0 |     0 |     1 |     1 |     1 |     1 |     1 |     1 |
    |     1 |     0 |     0 |     0 |     0 |     1 |     1 |     0 |
    |     2 |     1 |     0 |     1 |     1 |     0 |     1 |     1 |
    |     3 |     1 |     0 |     0 |     1 |     1 |     1 |     1 |
    |     4 |     1 |     1 |     0 |     0 |     1 |     1 |     0 |
    |     5 |     1 |     1 |     0 |     1 |     1 |     0 |     1 |
    |     6 |     1 |     1 |     1 |     1 |     1 |     0 |     1 |
    |     7 |     0 |     0 |     0 |     0 |     1 |     1 |     1 |
    |     8 |     1 |     1 |     1 |     1 |     1 |     1 |     1 |
    |     9 |     1 |     1 |     0 |     1 |     1 |     1 |     1 |
    |    10 |     1 |     1 |     1 |     0 |     1 |     1 |     1 |
    |    11 |     1 |     1 |     1 |     1 |     1 |     0 |     0 |
    |    12 |     0 |     1 |     1 |     1 |     0 |     0 |     1 |
    |    13 |     1 |     0 |     1 |     1 |     1 |     1 |     0 |
    |    14 |     1 |     1 |     1 |     1 |     0 |     0 |     1 |
    |    15 |     1 |     1 |     1 |     0 |     0 |     0 |     1 |
