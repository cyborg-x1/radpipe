Feature: Variable Manipulator Entry Parser

  Scenario Outline: Testing Entry Parser
    Given the variable "entry" is set to "<entry>"
    When the var_manip entry parser is run
    Then the index flags should be "<index_flags>"
    And the keys should be "<keys>"

    Examples: Wellformed Entries: <comment>
      | comment              | entry                     | index_flags | keys                |
      | Empty string         |                           |             |                     |
      | Root array           | [0]                       |           1 | 0                   |
      | Root array with dict | [0].a                     |          10 | 0,a                 |
      | Multiple Things      | [0].a[9].b.c.d[1][2].a[0] |  1010001101 | 0,a,9,b,c,d,1,2,a,0 |
      | Root element         | a                         |           0 | a                   |
      | Dict 3 elements      | a.b.c                     |         000 | a,b,c               |
      | Dict escaped dot     | \.a                       |           0 | .a                  |
      | Dict escaped [ tab   | \\a\[a\t                  |           0 | \\a[a\t             |
      | Dict escaped ] tab   | \\a\]a\t                  |           0 | \\a]a\t             |
      | Dict escaped dot tab | \\a\.a\t                  |           0 | \\a.a\t             |
      | Escaped Brackets     | a\[0\]                    |           0 | a[0]                |
      | AsciiArt Entry       | Robot.\[*\.*\]<Hi!        |          00 | Robot,[*.*]<Hi!     |

  Scenario Outline: Testing Entry Parser Syntax Errors
    Given the variable "entry" is set to "<entry>"
    When the var_manip entry parser is run
    Then "SyntaxError" exception should be raised

    Examples: Malformed Entries: <comment>
      | comment                  | entry      |
      | Trailing dot             | a.b.c.     |
      | Trailing [               | a.b.c[     |
      | Empty between dots       | a.b..c     |
      | No key between .[        | a.b.[0]    |
      | No dot between key and [ | a.b[0]c    |
      | Text in []               | a.b.c[d]   |
      | Dot in []                | a.b.c[9.9] |
      | Nothing in []            | a.b.c[]    |
      | Dangling ]               | a.b.c]     |
      | Only dot                 | .          |
      | Dot at start             | .a         |
      | No dot after []          | [9]s       |
      | Two dots after []        | [9]..s     |
      | Double []                | [[0]]      |

