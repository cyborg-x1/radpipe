Role Name
=========

Role to collect repository clone and ssh URLs

Requirements
------------

None

Role Variables
--------------

    - instance:
       URL of the service (if it is a private gitlab or github etc.)
    - type: 
      Type of organisational unit to query the repos from
       - gitlab:
          - user
          - group
       - github:
          - user
          - org
    - implementation: 
      Which kind of protocol
       - github
       - gitlab
    
    facts:
    - radpipe_collected_repos_http: list to create/append repo http urls to
    - radpipe_collected_repos_ssh: list to create/append repo ssh urls to

Dependencies
------------

- jmespath

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:


    - name: Collect github user repos
      include_role:
        name: radpipe-repo-collect
      vars:
        instance: github.com
        implementation: github
        type: user
        id: cyborg-x1

    - name: Collect github group repos
      include_role:
        name: radpipe-repo-collect
      vars:
        instance: github.com
        implementation: github
        type: org
        id: C-X1

    - name: Collect gitlab user repos
      include_role:
        name: radpipe-repo-collect
      vars:
        instance: gitlab.com
        implementation: gitlab
        type: user
        id: cyborg-x1

    - name: Collect gitlab group repos
      include_role:
        name: radpipe-repo-collect
      vars:
        instance: gitlab.com
        implementation: gitlab
        type: group
        id: C-X1

    - name: Output collected http repo links
      debug:
        var: radpipe_collected_repos_http
        
    - name: Output collected ssh repo links
      debug:
        var: radpipe_collected_repos_ssh 
    

License
-------

BSD

