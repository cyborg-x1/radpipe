###########
# RADPIPE #
###########
radpipe_local_compose_project: radpipe
radpipe_install_path: "{{ lookup('env','HOME') }}/.radpipe"
radpipe_cert_path: "{{ radpipe_install_path }}/certificates"
radpipe_key_path: "{{ radpipe_install_path }}/certificate_keys"
radpipe_config_path: "{{ radpipe_install_path }}/config"
radpipe_volumes_path: "{{ radpipe_install_path }}/volumes"
radpipe_local_ansible_vault: "{{ radpipe_install_path }}/secrets.yml"
radpipe_domain: "radpipe"


radpipe_networks:
  main:
    name: "main"
    subnet: "172.18.16.0/24"

rp_srv:
    concourse:
      hostname: "concourse.{{ radpipe_domain }}"
      container_name: "concourse.{{ radpipe_domain }}"
      image_tag: latest
      port: 8080
      local_user: radiating_pilot
      external_url: http://localhost:8080
      vault_shared_path: "all"

    grafana:
      image: grafana/loki
      hostname: "grafana.{{ radpipe_domain }}"
      container_name: "grafana.{{ radpipe_domain }}"
      image_tag: latest
      data_target_path: /loki
      data_path: "{{ radpipe_config_path }}/grafana_data"

    vault:
      container_name: "vault.{{ radpipe_domain }}"
      ip:
        main: "172.18.16.22"
      port: 8200
      image_tag: latest
      config: "{{ radpipe_config_path }}/vault.json"
      cert_public: "{{ radpipe_cert_path }}/vault.crt"
      cert_key: "{{ radpipe_key_path }}/vault.key"
      backend_path: "{{ radpipe_volumes_path }}/backend"
      storage_path: "{{ radpipe_volumes_path }}/storage"
      wait_for_volumes: 10 #Cert Key volume seems to need time to be avaiable
      #LOCAL SETUP
      local_key_shares: 1
      local_key_threshold: 1
      ##########

    postgres:
      container_name: "postgres.{{ radpipe_domain }}"
      hostname: "postgres.{{ radpipe_domain }}"
      image_tag: latest
      data_path: "{{ radpipe_volumes_path }}/postgres_data"
      db_name: "radpipe"
      root_user: "radpipe"
      vault_user: "vault"

vault_config:
  config_file:
    max_lease_ttl: 720h
    default_lease_ttl: 168h
    ui: true #Webui enabled
    backend:
      file:
        path: "/vault/backend"
    storage:
      file:
        path: "/vault/storage"

    listener:
      tcp:
        address: "0.0.0.0:{{ rp_srv.vault.port }}"
        tls_disable: 0
        tls_cert_file: "/etc/ssl/certs/vault.crt"
        tls_key_file: "/vault/vault.key"

    #telemetry:
    #  statsite_address: 172.20.0.6:8125
    #  disable_hostname: true


  secret_engines:
    - engine: kv
      path: concourse
      version: 1
    - engine: pki
      path: pki

  auths: []

  policies:
    - register_as: concourse
      policy_content:
        path:
          "concourse/*":
            policy: "read"

  tunes:
    - path: pki
      args:
        - arg: max-lease-ttl
          value: 87600h

  writes:    
    - |
      -field=certificate pki/root/generate/internal \
      common_name="{{ radpipe_domain }}" \
      ttl=87600h

    - |
      pki/config/urls \
      issuing_certificates="http://127.0.0.1:8200/v1/pki/ca" \
      crl_distribution_points="http://127.0.0.1:8200/v1/pki/crl"

    - |
      pki/roles/radpipe-local \
      allowed_domains="{{ radpipe_domain }}" \
      allow_subdomains=true \
      max_ttl="24h"


#DO NOT SET SECRETS HERE - these are just empty values for removing containers
secrets:
  postgres:
    root:
      user: ""
      pw: ""
  concourse:
    local:
      user: ""
      pw: ""

tmp_secrets:
  concourse:
    vault_token: ""
