Certs
=======

This role generates a selfsigned certificate including private key

Role Variables
--------------

* cert_name
  Base file name of all files, default is "certificate"

* cert_dir
  Directory to store certificate, default is ~/.certs/public

* key_dir
  Directory to store the key ~/.certs

* org
  Organisation name for the certificate

* key_end
  Directory to store the key, default: key
  
* csr_end
  File end of sign request, default: csr

* cert_end
  File end of the certificate, default: crt


Example Playbook
----------------

    - name: "Create certs"
      hosts: localhost

      tasks:
      - include_role:
          name: radpipe-certs
          vars:
            cert_name: "domain"
            cert_hostname: "localhost"
            cert_alt_names: 
              - "IP:10.0.0.1"
              - "DNS:acme"


License
-------

BSD
